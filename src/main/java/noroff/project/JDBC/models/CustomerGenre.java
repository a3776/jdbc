package noroff.project.JDBC.models;

public class CustomerGenre {
    private String name;
    private String[] favoriteGenres;

    public CustomerGenre(String name, String[] favoriteGenres) {
        this.name = name;
        this.favoriteGenres = favoriteGenres;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getFavoriteGenres() {
        return favoriteGenres;
    }

    public void setFavoriteGenres(String[] favoriteGenres) {
        this.favoriteGenres = favoriteGenres;
    }
}
