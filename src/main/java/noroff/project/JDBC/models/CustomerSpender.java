package noroff.project.JDBC.models;

public class CustomerSpender {
    private String customerName;
    private double totalSpent;

    public CustomerSpender(String customerName, double totalSpent) {
        this.customerName = customerName;
        this.totalSpent = totalSpent;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public double getTotalSpent() {
        return totalSpent;
    }

    public void setTotalSpent(double totalSpent) {
        this.totalSpent = totalSpent;
    }
}
