package noroff.project.JDBC.models;

public class CustomerCountry {
    private String customerCountry;
    private int numberOfCustomers;

    public CustomerCountry(int numberOfCustomers, String customerCountry) {
        this.customerCountry = customerCountry;
        this.numberOfCustomers = numberOfCustomers;
    }

    public int getNumberOfCustomers() {
        return numberOfCustomers;
    }

    public void setNumberOfCustomers(int numberOfCustomers) {
        this.numberOfCustomers = numberOfCustomers;
    }

    public String getCustomerCountry() {
        return customerCountry;
    }

    public void setCustomerCountry(String customerCountry) {
        this.customerCountry = customerCountry;
    }
}
