package noroff.project.JDBC.controller;

import noroff.project.JDBC.data_access.HomePageRepositoryImpl;
import noroff.project.JDBC.models.Song;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;

@Controller
public class ThymeleafController {
    @Autowired
    private final HomePageRepositoryImpl helper;

    public ThymeleafController(HomePageRepositoryImpl helper) {
        this.helper = helper;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Model model){
        ArrayList<String> artists = helper.getRandomArtists();
        ArrayList<String> songs = helper.getRandomSongs();
        ArrayList<String> genres = helper.getRandomGenres();
        model.addAttribute("artists", artists);
        model.addAttribute("songs", songs);
        model.addAttribute("genres", genres);
        Song song = new Song();
        model.addAttribute("song", song);
        return "mainPage";
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public String getSong(@RequestParam String name, Model model){
        Song song = helper.getSong(name);
        model.addAttribute("song", song);

        return "someDetail";
    }
}