package noroff.project.JDBC.controller;

import noroff.project.JDBC.data_access.CustomerRepository;
import noroff.project.JDBC.data_access.CustomerRepositoryImpl;
import noroff.project.JDBC.models.Customer;
import noroff.project.JDBC.models.CustomerCountry;
import noroff.project.JDBC.models.CustomerGenre;
import noroff.project.JDBC.models.CustomerSpender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class CustomerController {

    @Autowired
    private final CustomerRepository helper;

    public CustomerController(CustomerRepository customerRepository) {
        this.helper = customerRepository;
    }

    @GetMapping("api/customer")
    public ArrayList<Customer> customers(){
        return helper.selectAllCustomers();
    }

    @GetMapping("api/customer/{id}")
    public Customer getCustomerById(@PathVariable int id){
        return helper.getCustomer(id);
    }

    @GetMapping("api/customer/byname/{name}")
    public Customer getCustomerByName(@PathVariable String name){
        return helper.getCustomerByName(name);
    }


    @GetMapping("api/customer/{limit}/{offset}")
    public ArrayList<Customer> getCustomerSelection(@PathVariable int limit, @PathVariable int offset){
        return helper.getCustomerSelection(limit, offset);
    }

    @RequestMapping(value = "api/customer", method = RequestMethod.POST)
    public boolean newCustomer(@RequestBody Customer customer){
        return helper.newCustomer(customer);
    }

    @RequestMapping(value = "api/customer/{id}", method = RequestMethod.PUT)
    public boolean updatingCustomer(@PathVariable int id, @RequestBody Customer customer){
        return helper.updatingCustomer(id, customer);
    }

    @GetMapping("api/customer/country")
    public ArrayList<CustomerCountry> getCustomerFromCountry(){
        return helper.getCustomerFromCountry();
    }

    @GetMapping("api/customer/spent")
    public ArrayList<CustomerSpender> getHighestSpenders(){
        return helper.getHighestSpenders();
    }

    @GetMapping("api/customer/{id}/favoritegenres")
    public CustomerGenre getCustomerGenres(@PathVariable int id){
        return helper.getCustomerGenres(id);
    }

}
