package noroff.project.JDBC.data_access;

import noroff.project.JDBC.logging.Logger;
import noroff.project.JDBC.models.Customer;
import noroff.project.JDBC.models.CustomerCountry;
import noroff.project.JDBC.models.CustomerGenre;
import noroff.project.JDBC.models.CustomerSpender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository {

    @Autowired
    private final Logger logger;
    // Setup
    private String URL = ConnectionHelper.CONNECTION_URL;
    private Connection conn = null;

    public CustomerRepositoryImpl(Logger logger) {
        this.logger = logger;
    }

    @Override
    public ArrayList<Customer> selectAllCustomers(){
        ArrayList<Customer> customers = new ArrayList<Customer>();
        try {
            // Open Connection
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT CustomerId,FirstName,LastName,Country,PostalCode,Phone,Email FROM customer");
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            // Process Results
            while (resultSet.next()) {
                customers.add(
                        new Customer(
                                resultSet.getInt("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("Country"),
                                resultSet.getString("PostalCode"),
                                resultSet.getString("Phone"),
                                resultSet.getString("Email")
                        ));
            }
        }
        catch (Exception ex){
            logger.log("Something went wrong...");
            logger.log(ex.toString());
        }
        finally {
            try {
                // Close Connection
                conn.close();
            }
            catch (Exception ex){
                logger.log("Something went wrong while closing connection.");
                logger.log(ex.toString());
            }
            return customers;
        }
    }

    @Override
    public Customer getCustomer(int customerId){
        Customer customer = null;
        try {
            // Open Connection
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT CustomerId,FirstName,LastName,Country,PostalCode,Phone,Email FROM customer WHERE CustomerId = ?");
            preparedStatement.setString(1, String.valueOf(customerId)); // Corresponds to 1st '?' (must match type)
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            // Process Results
            while (resultSet.next()) {
                customer = new Customer(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                );
            }

        }
        catch (Exception ex){
            logger.log("Something went wrong...");
            logger.log(ex.toString());
        }
        finally {
            try {
                // Close Connection
                conn.close();
            }
            catch (Exception ex){
                logger.log("Something went wrong while closing connection.");
                logger.log(ex.toString());
            }
            return customer;
        }
    }

    @Override
    public Customer getCustomerByName(String name){
        Customer customer = null;
        try {
            // Open Connection
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT CustomerId,FirstName || ' ' || LastName as Name,Country,PostalCode,Phone,Email FROM customer WHERE Name LIKE ?");
            preparedStatement.setString(1, name); // Corresponds to 1st '?' (must match type)
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            // Process Results
            while (resultSet.next()) {
                String[] splitName = resultSet.getString("Name").split(" ");
                        customer = new Customer(
                        resultSet.getInt("CustomerId"),
                        splitName[0],
                        splitName[1],
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                );
            }

        }
        catch (Exception ex){
            logger.log("Something went wrong...");
            logger.log(ex.toString());
        }
        finally {
            try {
                // Close Connection
                conn.close();
            }
            catch (Exception ex){
                logger.log("Something went wrong while closing connection.");
                logger.log(ex.toString());
            }
        }
            return customer;

    }

    @Override
    public ArrayList<Customer> getCustomerSelection(int limit, int offset){
        ArrayList<Customer> customers = new ArrayList<Customer>();
        try {
            // Open Connection
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT CustomerId,FirstName,LastName,Country,PostalCode,Phone,Email FROM customer LIMIT ? OFFSET ?");
            preparedStatement.setString(1, String.valueOf(limit));
            preparedStatement.setString(2, String.valueOf(offset));
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            // Process Results
            while (resultSet.next()) {
                customers.add(
                        new Customer(
                                resultSet.getInt("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("Country"),
                                resultSet.getString("PostalCode"),
                                resultSet.getString("Phone"),
                                resultSet.getString("Email")
                        ));
            }
        }
        catch (Exception ex){
            logger.log("Something went wrong...");
            logger.log(ex.toString());
        }
        finally {
            try {
                // Close Connection
                conn.close();
            }
            catch (Exception ex){
                logger.log("Something went wrong while closing connection.");
                logger.log(ex.toString());
            }
            return customers;
        }
    }
    @Override
    public ArrayList<CustomerCountry> getCustomerFromCountry(){
        ArrayList<CustomerCountry> customerCountries = new ArrayList<CustomerCountry>();
        try {
            // Open Connection
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT COUNT(CustomerId) AS NumberOfCustomers, Country FROM customer GROUP BY Country ORDER BY NumberOfCustomers DESC");
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            // Process Results
            while (resultSet.next()) {
                customerCountries.add(
                        new CustomerCountry(
                                resultSet.getInt("NumberOfCustomers"),
                                resultSet.getString("Country")
                        ));
            }
        }
        catch (Exception ex){
            logger.log("Something went wrong...");
            logger.log(ex.toString());
        }
        finally {
            try {
                // Close Connection
                conn.close();
            }
            catch (Exception ex){
                logger.log("Something went wrong while closing connection.");
                logger.log(ex.toString());
            }
            return customerCountries;
        }
    }

    @Override
    public ArrayList<CustomerSpender> getHighestSpenders(){
        ArrayList<CustomerSpender> customerSpenders = new ArrayList<CustomerSpender>();
        try {
            // Open Connection
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT FirstName || ' ' || LastName AS Name, sum(UnitPrice) AS TotalSpent " +
                            "FROM invoice " +
                            "INNER JOIN InvoiceLine ON invoice.InvoiceId = InvoiceLine.InvoiceId " +
                            "INNER JOIN customer ON invoice.CustomerId = customer.CustomerId " +
                            "GROUP BY customer.CustomerId " +
                            "ORDER BY TotalSpent DESC");
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            // Process Results
            while (resultSet.next()) {
                customerSpenders.add(
                        new CustomerSpender(
                                resultSet.getString("Name"),
                                resultSet.getDouble("TotalSpent")
                        ));
            }
        }
        catch (Exception ex){
            logger.log("Something went wrong...");
            logger.log(ex.toString());
        }
        finally {
            try {
                // Close Connection
                conn.close();
            }
            catch (Exception ex){
                logger.log("Something went wrong while closing connection.");
                logger.log(ex.toString());
            }
            return customerSpenders;
        }
    }

    @Override
    public boolean newCustomer(Customer customer){
        boolean addingSuccess = false;
        try {
            // Open Connection
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement(
                            "INSERT INTO customer (FirstName, LastName, Country, PostalCode, Phone, Email) " +
                            "VALUES (?, ?, ?, ?, ?, ?) ");
            // Execute Statement
            preparedStatement.setString(1, customer.getFirstName());
            preparedStatement.setString(2, customer.getLastName());
            preparedStatement.setString(3, customer.getCountry());
            preparedStatement.setString(4, customer.getPostalcode());
            preparedStatement.setString(5, customer.getPhoneNumber());
            preparedStatement.setString(6, customer.getEmail());
            int result = preparedStatement.executeUpdate();
            addingSuccess = (result != 0);

            // Process Results
        }
        catch (Exception ex){
            logger.log("Something went wrong...");
            logger.log(ex.toString());
        }
        finally {
            try {
                // Close Connection
                conn.close();
            }
            catch (Exception ex){
                logger.log("Something went wrong while closing connection.");
                logger.log(ex.toString());
            }
            return addingSuccess;
        }
    }
    @Override
    public boolean updatingCustomer(int id, Customer customer){
        boolean addingSuccess = false;
        try {
            // Open Connection
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement(
                            "UPDATE customer SET FirstName = ?, LastName = ?, Country = ?, PostalCode = ?, Phone = ?, Email = ? " +
                                    "WHERE customerId=? ");
            // Execute Statement
            preparedStatement.setString(1, customer.getFirstName());
            preparedStatement.setString(2, customer.getLastName());
            preparedStatement.setString(3, customer.getCountry());
            preparedStatement.setString(4, customer.getPostalcode());
            preparedStatement.setString(5, customer.getPhoneNumber());
            preparedStatement.setString(6, customer.getEmail());
            preparedStatement.setString(7, String.valueOf(id));
            int result = preparedStatement.executeUpdate();
            addingSuccess = (result != 0);

            // Process Results
        }
        catch (Exception ex){
            logger.log("Something went wrong...");
            logger.log(ex.toString());
        }
        finally {
            try {
                // Close Connection
                conn.close();
            }
            catch (Exception ex){
                logger.log("Something went wrong while closing connection.");
                logger.log(ex.toString());
            }
            return addingSuccess;
        }
    }

    @Override
    public CustomerGenre getCustomerGenres(int id){
        CustomerGenre currentCustomer = null;
        try {
            // Open Connection
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT CustomerName, GenreName, max(GenreTotal), CustomerId FROM " +
                            "(SELECT FirstName || ' ' || LastName AS CustomerName, count(genre.genreId) AS GenreTotal, genre.Name AS GenreName, customer.customerId AS CustomerId " +
                            "FROM invoice " +
                            "INNER JOIN InvoiceLine ON invoice.InvoiceId = InvoiceLine.InvoiceId " +
                            "INNER JOIN customer ON invoice.CustomerId = customer.CustomerId " +
                            "INNER JOIN track ON invoiceline.trackId = track.trackId " +
                            "INNER JOIN genre ON genre.genreId = track.genreId " +
                            "WHERE customer.CustomerId = ? " +
                            "GROUP BY genre.genreId) " );
            // Execute Statement
            preparedStatement.setString(1, String.valueOf(id));
            ResultSet resultSet = preparedStatement.executeQuery();

            // Process Results
            ArrayList<String> genres = new ArrayList<>();
            String customerName = "";
            while (resultSet.next()) {
                                customerName = resultSet.getString("CustomerName");
                                genres.add(resultSet.getString("GenreName"));
            }
            String[] genresArray = new String[genres.size()];
            for (int i = 0; i < genres.size(); i++) {
                genresArray[i] = genres.get(i);
            }
            currentCustomer = new CustomerGenre(customerName, genresArray);
        }
        catch (Exception ex){
            logger.log("Something went wrong...");
            logger.log(ex.toString());
        }
        finally {
            try {
                // Close Connection
                conn.close();
            }
            catch (Exception ex){
                logger.log("Something went wrong while closing connection.");
                logger.log(ex.toString());
            }
            return currentCustomer;
        }
    }

    @Override
    public Boolean addNewCustomer(Customer customer){
        return false;
    }

    @Override
    public Boolean updateExistingCustomer(Customer updatedCustomer){
        return false;
    }

}


