package noroff.project.JDBC.data_access;

import noroff.project.JDBC.models.Song;

import java.util.ArrayList;

public interface HomePageRepository {
    ArrayList<String> getRandomArtists();

    ArrayList<String> getRandomSongs();

    ArrayList<String> getRandomGenres();

    Song getSong(String searchName);
}
