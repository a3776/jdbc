package noroff.project.JDBC.data_access;

import noroff.project.JDBC.models.Customer;
import noroff.project.JDBC.models.CustomerCountry;
import noroff.project.JDBC.models.CustomerGenre;
import noroff.project.JDBC.models.CustomerSpender;

import java.util.ArrayList;

public interface CustomerRepository {
    ArrayList<Customer> selectAllCustomers();

    Customer getCustomer(int customerId);

    Customer getCustomerByName(String name);

    ArrayList<Customer> getCustomerSelection(int limit, int offset);

    ArrayList<CustomerCountry> getCustomerFromCountry();

    ArrayList<CustomerSpender> getHighestSpenders();

    boolean newCustomer(Customer customer);

    boolean updatingCustomer(int id, Customer customer);

    CustomerGenre getCustomerGenres(int id);

    Boolean addNewCustomer(Customer customer);

    Boolean updateExistingCustomer(Customer updatedCustomer);
}
