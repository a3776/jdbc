package noroff.project.JDBC.data_access;

import noroff.project.JDBC.logging.Logger;
import noroff.project.JDBC.models.Customer;
import noroff.project.JDBC.models.Song;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Random;

@Repository
public class HomePageRepositoryImpl implements HomePageRepository {

    @Autowired
    private final Logger logger;
    private String URL = ConnectionHelper.CONNECTION_URL;
    private Connection conn = null;
    private Random random = new Random();

    public HomePageRepositoryImpl(Logger logger) {
        this.logger = logger;
    }

    @Override
    public ArrayList<String> getRandomArtists(){
        ArrayList<String> artists = new ArrayList<String>();
        try {
            // Open Connection
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT max(ArtistId) as MaxId FROM Artist");
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            int maxId = resultSet.getInt("MaxId") - 5;
            int startId = random.nextInt(maxId + 1);

            // Prepare Statement
            preparedStatement =
                    conn.prepareStatement("SELECT Name FROM Artist LIMIT 5 OFFSET ?");
            preparedStatement.setString(1, String.valueOf(startId));
            // Execute Statement
            resultSet = preparedStatement.executeQuery();

            // Process Results
            while (resultSet.next()) {
                artists.add(resultSet.getString("Name"));
            }
        }
        catch (Exception ex){
            logger.log("Something went wrong...");
            logger.log(ex.toString());
        }
        finally {
            try {
                // Close Connection
                conn.close();
            }
            catch (Exception ex){
                logger.log("Something went wrong while closing connection.");
                logger.log(ex.toString());
            }
            return artists;
        }
    }

    @Override
    public ArrayList<String> getRandomSongs(){
        ArrayList<String> songs = new ArrayList<String>();
        try {
            // Open Connection
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT max(TrackId) as MaxId FROM Track");
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            int maxId = resultSet.getInt("MaxId") - 5;
            int startId = random.nextInt(maxId + 1);

            // Prepare Statement
            preparedStatement =
                    conn.prepareStatement("SELECT Name FROM Track LIMIT 5 OFFSET ? ");
            preparedStatement.setString(1, String.valueOf(startId));
            // Execute Statement
            resultSet = preparedStatement.executeQuery();

            // Process Results
            while (resultSet.next()) {
                songs.add(resultSet.getString("Name"));
            }
        }
        catch (Exception ex){
            logger.log("Something went wrong...");
            logger.log(ex.toString());
        }
        finally {
            try {
                // Close Connection
                conn.close();
            }
            catch (Exception ex){
                logger.log("Something went wrong while closing connection.");
                logger.log(ex.toString());
            }
            return songs;
        }
    }

    @Override
    public ArrayList<String> getRandomGenres(){
        ArrayList<String> genres = new ArrayList<String>();
        try {
            // Open Connection
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT max(GenreId) as MaxId FROM Genre");
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            int maxId = resultSet.getInt("MaxId") - 5;
            int startId = random.nextInt(maxId + 1);

            // Prepare Statement
            preparedStatement =
                    conn.prepareStatement("SELECT Name FROM Genre LIMIT 5 OFFSET ?");
            preparedStatement.setString(1, String.valueOf(startId));
            // Execute Statement
            resultSet = preparedStatement.executeQuery();

            // Process Results
            while (resultSet.next()) {
                genres.add(resultSet.getString("Name"));
            }
        }
        catch (Exception ex){
            logger.log("Something went wrong...");
            logger.log(ex.toString());
        }
        finally {
            try {
                // Close Connection
                conn.close();
            }
            catch (Exception ex){
                logger.log("Something went wrong while closing connection.");
                logger.log(ex.toString());
            }
            return genres;
        }
    }

    @Override
    public Song getSong(String searchName){
        Song song = null;
        try {
            // Open Connection
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("Select Track.name AS Name, Artist.name AS Artist, Album.title AS Album, Genre.name AS Genre " +
                                    "FROM Track " +
                                    "INNER JOIN Genre ON Track.genreId = Genre.genreid " +
                                    "Inner join Album ON Track.albumid = Album.albumid " +
                                    "INNER JOIN Artist ON Album.artistid = Artist.artistid " +
                                    "WHERE Track.name LIKE ? ");
            preparedStatement.setString(1, searchName);
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            // Process Results
            while (resultSet.next()) {
                song = new Song(resultSet.getString("Name"),
                        resultSet.getString("Artist"),
                        resultSet.getString("Album"),
                        resultSet.getString("Genre"));
            }
        }
        catch (Exception ex){
            song = new Song("N/a", "N/a","N/a", "N/a");
            logger.log("No song found :( ");
        }
        finally {
            try {
                // Close Connection
                conn.close();
            }
            catch (Exception ex){
                logger.log("Something went wrong while closing connection.");
                logger.log(ex.toString());
            }
            return song;
        }
    }
}
