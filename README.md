#JDBC project
##Description
This application is used to get information from a SQLite database locally.
It can also be used to post and update customers in the database.

##Usage
The home page displays five random artists, songs, and genres from the database, and provides a search bar where you can search for songs by their title.
To get all customers use the path: api/customer
To get a customer by id use the path: api/customer/{id}
To get a customer by name use the path: api/customer/byname/{name}
To get a limited choice of customers with a specific offset use the path: api/customer/{limit}/{offset}
To get the total amount of customers per country use the path: api/customer/country
To get a customer's favorite genre use the path api/customer/{id}/favoritegenres
To add new a customer, use POST in Postman with the path: api/customer.
To update a customer, use POST in Postman with the path: api/customer/{id}. 
