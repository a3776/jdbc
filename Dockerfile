FROM openjdk:16
ADD target/JDBC-0.0.1-SNAPSHOT.jar jarfile.jar
ENTRYPOINT [ "java", "-jar", "jarfile.jar" ]